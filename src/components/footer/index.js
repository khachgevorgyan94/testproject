import React, {Component} from 'react';
import './style.scss';
import './responsive.scss';
import {ReactSVG} from 'react-svg'
import footerLogoSvg from '../../assets/images/SVG/footer-logo.svg'


class Footer extends Component {

    render() {
        return (
         <footer>
             <div className="G-container">
                 <div className="P-footer-block G-flex G-align-center G-justify-between">
                     <p>© Копирайт 2019</p>
                     <ReactSVG src={footerLogoSvg}  />
                 </div>
             </div>

         </footer>

        );
    }
};

export default Footer;
