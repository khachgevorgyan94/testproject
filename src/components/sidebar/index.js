import React, {Component} from 'react';

import './style.scss';
import './responsive.scss';
import ROUTES from "../../platform/constants/routes";
import {Link, NavLink} from "react-router-dom";
import {ReactSVG} from 'react-svg'
import instaSVG from '../../assets/images/SVG/insta.svg'
import vkSVG from '../../assets/images/SVG/vk.svg'
import phoneSVG from '../../assets/images/SVG/phone.svg'
import logoSVG from '../../assets/images/SVG/logo.svg'



class Sidebar extends Component {

    state = {
        isOpenMenu:false,
        addStyle:false,
    }

    componentDidMount() {

        window.addEventListener('scroll', this.addStyleSidebar)
    }
    addStyleSidebar = ()=>{
        if(window.scrollY>0){
            this.setState({addStyle:true})
        }else{
            this.setState({addStyle:false})

        }
    }
    toggleOpenMenu = ()=>{
        this.setState({isOpenMenu:!this.state.isOpenMenu})
        if(this.state.isOpenMenu){
            document.body.classList.remove('body-fixed')

        }else{
            document.body.classList.add('body-fixed')

        }


    }


    render() {
        const {isOpenMenu, addStyle} = this.state
        return (
            <header className={`G-flex G-align-center ${isOpenMenu? 'header-open-menu':''} ${addStyle ? 'add-fixed-style':''}`}>
                <div className="G-container">
                    <div className="sidenav-block G-flex G-justify-between">
                        <div className="P-logo">
                            <Link to={ROUTES.HOME}>
                                <ReactSVG src={logoSVG}
                                          beforeInjection={svg => {
                                              if(isOpenMenu || addStyle){
                                                  svg.classList.add('svg-class-name')
                                              }
                                          }}/>
                            </Link>
                        </div>
                        <div className="P-header-info G-flex G-align-center">
                            <div className="P-header-call-back">
                                <a href="tel:8 499 999 99 99" className="G-flex G-align-center">
                                    <ReactSVG src={phoneSVG}/>
                                    <span>8 499 999 99 99</span>
                                </a>
                            </div>
                            <div className="P-header-soc-logins">
                                <ul className="G-flex G-align-center">
                                    <li>
                                        <a href='https://www.instagram.com/' target='_blank' className='P-insta-icon'>
                                            <ReactSVG src={instaSVG}/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='https://vk.com/' target='_blank' className='P-vk-icon'>
                                            <ReactSVG src={vkSVG}/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="open-menu" onClick={this.toggleOpenMenu}>
                                <span/>
                                <span/>
                            </div>
                        </div>

                    </div>
                </div>

                {/*   Menu List */}
                <div className={`P-menu-block ${isOpenMenu? 'open-menu-block':''}`} >
                    <div className="P-menu-list G-flex G-flex-column G-align-center">
                        <div className="P-header-call-back">
                            <a href="tel:8 499 999 99 99" className="G-flex G-align-center">
                                <ReactSVG src={phoneSVG}/>
                                <span>8 499 999 99 99</span>
                            </a>
                        </div>
                        <ul>
                            <li>
                                <NavLink to={ROUTES.HOME}>
                                    пункт меню 1
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={ROUTES.HOME}>
                                    пункт меню 2
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={ROUTES.HOME}>
                                    пункт меню 3
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={ROUTES.HOME}>
                                    пункт меню 4
                                </NavLink>
                            </li>

                        </ul>
                        <div className="P-header-soc-logins">
                            <ul className="G-flex G-align-center">
                                <li>
                                    <a href='https://www.instagram.com/' target='_blank' className='P-insta-icon'>
                                        <ReactSVG src={instaSVG}/>
                                    </a>
                                </li>
                                <li>
                                    <a href='https://vk.com/' target='_blank' className='P-vk-icon'>
                                        <ReactSVG src={vkSVG}/>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>



            </header>

        );
    }
};

export default Sidebar;
