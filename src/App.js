import React, {Component} from 'react';
import {createBrowserHistory} from 'history';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import {
  Home
} from './modules';
import ROUTES from './platform/constants/routes';
import './assets/fonts/stylesheet.css'
import './assets/styles/index.scss';


class App extends Component {

    state = {
         generalAPILoaded: false,
        initialStorageFetched: true,
        isLoad: false,
        closeHeaderAndFooter: true,
    };

    componentDidMount() {
        //? For SSR to fully load Browser API (general for 'window')
        window.routerHistory = createBrowserHistory();
        window.routerHistory.listen(() => window.scrollTo(0, 0));
        this.setState({generalAPILoaded: true});
        // ? Backend initial data fetch

    }


    render() {
        return  (
            <BrowserRouter>
                    <Switch>
                        <Route exact path={ROUTES.HOME} component={Home} />
                        <Redirect to={ROUTES.HOME} component={Home} />
                    </Switch>
            </BrowserRouter>
        )
    }
}

export default App;
