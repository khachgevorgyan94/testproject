import React, {Component} from 'react';
import './style.scss';
import './responsive.scss';

import Sidebar from "../../components/sidebar";
import Cover from "./components/cover";
import SliderBlock from "./components/slider-block";
import Company from "./components/company";
import Footer from "../../components/footer";

class Home extends Component {

    render() {
        return (
            <>
                <Sidebar/>
                <Cover/>
                <SliderBlock/>
                <Company/>
                <Footer/>
            </>

        );
    }
};

export default Home;
