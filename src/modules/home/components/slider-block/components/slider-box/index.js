import React, {Component} from 'react';
import sliderImg from "../../../../../../assets/images/slider-img.png";


class SliderBox extends Component {



    render() {
        return (
            <div className="P-slider-components">
                <div className="P-slider-section G-flex G-align-center">
                    <div className="P-left-path">
                        <h2>лоремова</h2>
                        <h3>ипсума анатольевна</h3>
                        <span>lorem ipsum dolor</span>

                        <div className="P-slider-information">
                            <ul>
                                <li>Lorem ipsum dolor.</li>
                                <li>Lorem ipsum dolor.</li>
                                <li>Lorem ipsum dolor.</li>
                                <li>Lorem ipsum dolor.</li>
                                <li>Lorem ipsum dolor.</li>
                                <li>Lorem ipsum dolor.</li>
                            </ul>
                            <h3>lorem ipsum dolor</h3>
                            <p>lorem ipsum dolorlorem ipsum dolorlorem ipsum
                                dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum
                                dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolorlorem ipsum dolor</p>
                            <h3>lorem ipsum dolor</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dolor illum libero molestiae, nihil placeat porro quis sapiente veritatis vitae?</p>
                        </div>
                    </div>
                    <div className="P-right-path">
                        <div className="P-slider-img" style={{backgroundImage:`url(${sliderImg})`}}/>
                    </div>
                </div>
            </div>

        );
    }
};

export default SliderBox;
