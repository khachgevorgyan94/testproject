import React, {Component} from 'react';
import './style.scss';
import './responsive.scss';
import Slider from "react-slick";
import SliderBox from "./components/slider-box";





class SliderBlock extends Component {
state = {
    currentSlider:0
}

     settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
         afterChange: (slider)=>{
            this.setState({currentSlider:slider})
         }


    };
     componentDidMount() {
         console.log(this.slider.props.children.length)
     }

    render() {
        return (
         <section>
             <div className="G-container">
                 <div className="P-slider-block">
                     <div className="P-slider-tittle">
                         <h3>второй блок</h3>
                     </div>
                     <div className="P-slider-box">
                         <Slider {...this.settings} ref={slider => (this.slider = slider)}>
                             <SliderBox/>
                             <SliderBox/>
                             <SliderBox/>
                             <SliderBox/>
                             <SliderBox/>
                         </Slider>

                     </div>
                     {this.slider ?  <div className='P-slider-count'><p>{this.state.currentSlider+1} из {this.slider.props.children.length}</p></div>
                         : null}
                 </div>
             </div>
         </section>

        );
    }
};

export default SliderBlock;
