import React, {Component} from 'react';



class CompanyBox extends Component {

    render() {
        const {img, title, text} = this.props
        return (
            <div className="P-padding-company">
                <div className="P-company-box">
                    <div className="P-company-img-block" style={{backgroundImage:`url(${img})`}}/>
                    <div className="P-company-information">
                        <h3>{title}</h3>
                        <p>{text}</p>
                    </div>
                </div>
            </div>
        );
    }
};

export default CompanyBox;
