import React, {Component} from 'react';
import './style.scss';
import './responsive.scss';
import company1Img from "../../../../assets/images/company-1.png";
import company2Img from "../../../../assets/images/company-2.png";
import company3Img from "../../../../assets/images/company-3.png";
import company4Img from "../../../../assets/images/company-4.png";

import CompanyBox from "./components/company-box";


class Company extends Component {

    render() {
        return (
          <section className="P-company-section">
              <div className="G-container">
                  <div className="P-company-block">
                      <div className="P-company-title">
                          <h2>третий блок</h2>
                      </div>
                      <div className="P-company-list G-flex G-flex-wrap">
                          <CompanyBox img={company1Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                          <CompanyBox img={company2Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                          <CompanyBox img={company3Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                          <CompanyBox img={company4Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                          <CompanyBox img={company1Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                          <CompanyBox img={company2Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                          <CompanyBox img={company3Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                          <CompanyBox img={company4Img} title='Lorem ipsum dolor sit amet.'  text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in nulla interdum felis placerat vulputate.' />
                      </div>
                  </div>
              </div>
          </section>

        );
    }
};

export default Company;
