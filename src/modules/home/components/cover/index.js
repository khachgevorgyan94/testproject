import React, {Component} from 'react';
import './style.scss';
import './responsive.scss';

import coverIMG from '../../../../assets/images/cover.jpg'

import {ReactSVG} from 'react-svg'

import prizeSVG from '../../../../assets/images/SVG/prize.svg'
import prize1SVG from '../../../../assets/images/SVG/prize-1.svg'
import prize2SVG from '../../../../assets/images/SVG/prize-2.svg'
import earthSVG from '../../../../assets/images/SVG/earth.svg'
import earth1SVG from '../../../../assets/images/SVG/earth-1.svg'
import earth2SVG from '../../../../assets/images/SVG/earth-3.svg'

class Cover extends Component {

    render() {
        return (
         <section>
             <div className="P-cover-page" style={{backgroundImage:`url(${coverIMG})`}}>
                 <div className="P-cover-title">
                     <div className="G-container">
                         <h1>lorem ipsum dolor sit amet</h1>
                         <p><span>lorem</span> ipsum dolor sit amet</p>
                     </div>
                 </div>
                 <div className="G-container">
                     <div className='P-cover-information'>
                         <div className="P-cover-info-bock">
                             <ul>
                                 <li>
                                     <div className="P-svg-block G-flex G-align-center">
                                         <ReactSVG src={prizeSVG}
                                                   beforeInjection={svg => {
                                                 svg.classList.add('svg-prize')
                                         }}/>
                                         <ReactSVG src={earthSVG}
                                                   beforeInjection={svg => {
                                                       svg.classList.add('svg-earth')
                                                   }}/>
                                     </div>
                                     <h3>Lorem ipsum dol</h3>
                                 </li>
                                 <li>
                                     <div className="P-svg-block G-flex G-align-center">
                                         <ReactSVG src={prize1SVG}
                                                   beforeInjection={svg => {
                                                       svg.classList.add('svg-prize')
                                                   }}/>
                                         <ReactSVG src={earth1SVG}
                                                   beforeInjection={svg => {
                                                       svg.classList.add('svg-earth')
                                                   }}/>
                                     </div>
                                     <p>lorem ipsum</p>
                                 </li>
                                 <li>
                                     <div className="P-svg-block G-flex G-align-center">
                                         <ReactSVG src={prize2SVG}
                                                   beforeInjection={svg => {
                                                       svg.classList.add('svg-prize-1')
                                                   }}/>
                                         <ReactSVG src={earth1SVG}
                                                   beforeInjection={svg => {
                                                       svg.classList.add('svg-earth')
                                                   }}/>
                                     </div>
                                     <p>lorem ipsum dolor sit amet</p>
                                 </li>
                                 <li>
                                     <div className="P-svg-block G-flex G-align-center">
                                         <ReactSVG src={prize1SVG}
                                                   beforeInjection={svg => {
                                                       svg.classList.add('svg-prize')
                                                   }}/>
                                         <ReactSVG src={earth2SVG}
                                                   beforeInjection={svg => {
                                                       svg.classList.add('svg-earth')
                                                   }}/>
                                     </div>
                                     <p>lorem ipsum</p>
                                 </li>
                             </ul>
                         </div>
                         <div className="P-cover-button G-btn">
                             <button>lorem ipsum dolor</button>
                         </div>
                     </div>
                 </div>
             </div>
         </section>

        );
    }
};

export default Cover;
